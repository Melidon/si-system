use core::marker::ConstParamTy;

#[derive(Clone, Copy, Eq, PartialEq, Debug, Hash, ConstParamTy)]
pub struct Dimension {
    pub second: i8,
    pub meter: i8,
    pub kilogram: i8,
    pub ampere: i8,
    pub kelvin: i8,
}

impl Dimension {
    pub const fn default() -> Self {
        Self {
            second: 0,
            meter: 0,
            kilogram: 0,
            ampere: 0,
            kelvin: 0,
        }
    }

    pub const fn add(self, rhs: Self) -> Self {
        Self {
            second: self.second + rhs.second,
            meter: self.meter + rhs.meter,
            kilogram: self.kilogram + rhs.kilogram,
            ampere: self.ampere + rhs.ampere,
            kelvin: self.kelvin + rhs.kelvin,
        }
    }

    pub const fn sub(self, rhs: Self) -> Self {
        Self {
            second: self.second - rhs.second,
            meter: self.meter - rhs.meter,
            kilogram: self.kilogram - rhs.kilogram,
            ampere: self.ampere - rhs.ampere,
            kelvin: self.kelvin - rhs.kelvin,
        }
    }
}

impl core::fmt::Display for Dimension {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let mut first = true;
        if self.kilogram != 0 {
            if !first {
                write!(f, "*")?;
            }
            first = false;
            write!(f, "kg")?;
            if self.kilogram != 1 {
                write!(f, "^{}", self.kilogram)?;
            }
        }
        if self.meter != 0 {
            if !first {
                write!(f, "*")?;
            }
            first = false;
            write!(f, "m")?;
            if self.meter != 1 {
                write!(f, "^{}", self.meter)?;
            }
        }
        if self.second != 0 {
            if !first {
                write!(f, "*")?;
            }
            first = false;
            write!(f, "s")?;
            if self.second != 1 {
                write!(f, "^{}", self.second)?;
            }
        }
        if self.ampere != 0 {
            if !first {
                write!(f, "*")?;
            }
            first = false;
            write!(f, "A")?;
            if self.ampere != 1 {
                write!(f, "^{}", self.ampere)?;
            }
        }
        if self.kelvin != 0 {
            if !first {
                write!(f, "*")?;
            }
            first = false;
            write!(f, "K")?;
            if self.kelvin != 1 {
                write!(f, "^{}", self.kelvin)?;
            }
        }
        Ok(())
    }
}
