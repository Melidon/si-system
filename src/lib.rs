#![no_std]
#![feature(adt_const_params)]
#![feature(generic_const_exprs)]
// #![feature(const_trait_impl)]

mod dimension;
pub use dimension::Dimension;

mod quantity;
pub use quantity::*;
