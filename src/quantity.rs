use {
    crate::Dimension,
    core::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign},
};

#[derive(Clone, Copy, PartialEq, Debug, Default)]
pub struct Quantity<const DIMENSION: Dimension> {
    magnitude: f64,
}

impl<const DIMENSION: Dimension> Add for Quantity<DIMENSION> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self::Output {
            magnitude: self.magnitude + rhs.magnitude,
        }
    }
}

impl<const DIMENSION: Dimension> AddAssign for Quantity<DIMENSION> {
    fn add_assign(&mut self, rhs: Self) {
        self.magnitude += rhs.magnitude
    }
}

impl<const DIMENSION: Dimension> Sub for Quantity<DIMENSION> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self::Output {
            magnitude: self.magnitude - rhs.magnitude,
        }
    }
}

impl<const DIMENSION: Dimension> SubAssign for Quantity<DIMENSION> {
    fn sub_assign(&mut self, rhs: Self) {
        self.magnitude -= rhs.magnitude;
    }
}

impl<const DIMENSION_1: Dimension, const DIMENSION_2: Dimension> Mul<Quantity<DIMENSION_2>>
    for Quantity<DIMENSION_1>
where
    Quantity<{ DIMENSION_1.add(DIMENSION_2) }>: Sized,
{
    type Output = Quantity<{ DIMENSION_1.add(DIMENSION_2) }>;

    fn mul(self, rhs: Quantity<DIMENSION_2>) -> Self::Output {
        Self::Output {
            magnitude: self.magnitude * rhs.magnitude,
        }
    }
}

impl<const DIMENSION: Dimension> Mul<f64> for Quantity<DIMENSION> {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self::Output {
        Self::Output {
            magnitude: self.magnitude * rhs,
        }
    }
}

impl<const DIMENSION: Dimension> MulAssign<f64> for Quantity<DIMENSION> {
    fn mul_assign(&mut self, rhs: f64) {
        self.magnitude *= rhs;
    }
}

impl<const DIMENSION_1: Dimension, const DIMENSION_2: Dimension> Div<Quantity<DIMENSION_2>>
    for Quantity<DIMENSION_1>
where
    Quantity<{ DIMENSION_1.sub(DIMENSION_2) }>: Sized,
{
    type Output = Quantity<{ DIMENSION_1.sub(DIMENSION_2) }>;

    fn div(self, rhs: Quantity<DIMENSION_2>) -> Self::Output {
        Self::Output {
            magnitude: self.magnitude / rhs.magnitude,
        }
    }
}

impl<const DIMENSION: Dimension> Div<f64> for Quantity<DIMENSION> {
    type Output = Self;

    fn div(self, rhs: f64) -> Self::Output {
        Self::Output {
            magnitude: self.magnitude / rhs,
        }
    }
}

impl<const DIMENSION: Dimension> DivAssign<f64> for Quantity<DIMENSION> {
    fn div_assign(&mut self, rhs: f64) {
        self.magnitude /= rhs;
    }
}

impl<const DIMENSION: Dimension> core::fmt::Display for Quantity<DIMENSION> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}{DIMENSION}", self.magnitude)
    }
}

mod base_units;
pub use base_units::*;
