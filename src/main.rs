#![feature(generic_const_exprs)]

use units::{Length, Time};

fn main() {
    let time = Time::from_hour(3.0);
    let length = Length::from_kilometer(108.0);
    let speed = length / time;

    println!("{speed}");
}
