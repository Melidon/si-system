use crate::{Dimension, Quantity};

pub type Time = Quantity<
    {
        Dimension {
            second: 1,
            ..Dimension::default()
        }
    },
>;

impl Time {
    pub fn from_second(second: f64) -> Time {
        Time { magnitude: second }
    }
    pub fn from_minute(minute: f64) -> Time {
        Time {
            magnitude: minute * 60.0,
        }
    }
    pub fn from_hour(hour: f64) -> Time {
        Time {
            magnitude: hour * 3600.0,
        }
    }
}

pub type Length = Quantity<
    {
        Dimension {
            meter: 1,
            ..Dimension::default()
        }
    },
>;

impl Length {
    pub fn from_meter(meter: f64) -> Length {
        Length { magnitude: meter }
    }
    pub fn from_kilometer(kilometer: f64) -> Length {
        Length {
            magnitude: kilometer * 1000.0,
        }
    }
}

pub type Mass = Quantity<
    {
        Dimension {
            kilogram: 1,
            ..Dimension::default()
        }
    },
>;

impl Mass {
    pub fn from_kilogram(kilogram: f64) -> Mass {
        Mass {
            magnitude: kilogram,
        }
    }
}

pub type Current = Quantity<
    {
        Dimension {
            ampere: 1,
            ..Dimension::default()
        }
    },
>;

impl Current {
    pub fn from_ampere(ampere: f64) -> Current {
        Current { magnitude: ampere }
    }
}

pub type Temperature = Quantity<
    {
        Dimension {
            kelvin: 1,
            ..Dimension::default()
        }
    },
>;

impl Temperature {
    pub fn from_kelvin(kelvin: f64) -> Temperature {
        Temperature { magnitude: kelvin }
    }

    pub fn from_celsius(celsius: f64) -> Temperature {
        Temperature {
            magnitude: celsius + 273.15,
        }
    }
}
